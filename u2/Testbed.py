#!/usr/bin/python3
import random
from time import sleep
from Task import Task, Message, Loadgenerator
from sched import scheduler


def guard_1(msg, task):
    return msg.data == 'nil'

def action_1(msg, task):
    if task.in_n0:
        dst = random.choice(list(task.out))
        task.send(Message(task, dst, 'token'), dst)

def guard_2(msg, task):
    return msg.data == 'load' and msg.origin.id == 'LG'

def action_2(msg, task):
    task.CR_needed = True

def guard_3(msg, task):
    return msg.data == 'token' and msg.origin.id != 'LG' and msg.origin in task._in and task.CR_needed #whenever CR needed..??

def action_3(msg, task):
    print('Task {} starts doing something in critical region.'.format(task.id))
    sleep(2)
    print('Task {} finished doing something in critical region.'.format(task.id))
    task.CR_needed = False
    dst = random.choice(list(task.out))
    task.send(Message(task, dst, 'token'), dst)


def guard_4(msg, task):
    return msg.data == 'token' and msg.origin.id != 'LG' and msg.origin in task._in and not task.CR_needed #whenever not CR needed..??

def action_4(msg, task):
    dst = random.choice(list(task.out))
    task.send(Message(task, dst, 'token'), dst)




def main():
    sched = scheduler()
    input_actions = [(guard_1, action_1), (guard_2, action_2), (guard_3, action_3), (guard_4, action_4)]
    n = 3
    tasks = []
    for i in range(n):
        tasks.append(Task(i, input_actions, sched))
        if i == 0:
            tasks[i].active_msg = Message(tasks[0], tasks[0], 'nil')
            tasks[i].in_n0 = True
        else:
            tasks[i].active_msg = Message(tasks[0], tasks[0], None)
    LG = Loadgenerator(rate = 3, out = tasks)
    for ti in tasks:
        to_connect = set()
        for tj in tasks:
            if ti.id != tj.id:
                to_connect.add(tj)
        ti._in = to_connect.copy()
        ti._in.add(LG)
        ti.out = to_connect.copy()
        ti.start()
    LG.start()
    LG.join()
    for t in tasks:
        t.join()




if __name__ == '__main__':
    main()
