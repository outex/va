#!/usr/bin/python3
import random
from time import sleep
from threading import Thread
from queue import Queue
from sched import scheduler

class Task(Thread):
    def __init__(self, id, input_actions, scheduler, _in = None, out= None, in_n0 = False, active_msg = None, CR_needed = False, task_active = True, args = (), kwargs= None):
        Thread.__init__(self, args = args, kwargs = kwargs, name = id)
        self.id = id
        self.input_actions = input_actions
        self._in = _in
        self.out = out
        self.in_n0 = in_n0
        self.active_msg = active_msg
        self.queue = Queue()
        self.scheduler = scheduler
        self.task_active = task_active

        self.CR_needed = CR_needed

    def run(self):
        print('Task {} is now active.'.format(self.id))
        while True:
            #print('Task {} msg: {}'.format(self.id, self.active_msg.data))
            if self.active_msg.data == None and not self.queue.empty():
                self.active_msg = self.queue.get()
            if self.active_msg.data != None:
                valid = self.check_inputs()
                if len(valid) > 0:
                    self.schedule(random.choice(valid))
            sleep(1)

    def send(self, msg, dst):
        dst.receive(msg)
        #print('Task {} sending {} to Task {}.'.format(self.id, msg.data, dst.id))

    def receive(self, msg):
        self.queue.put(msg)
        print('Task {} received {} from Task {}.'.format(self.id, msg.data, msg.origin.id))

    def check_inputs(self):
        #print('Task {} is now checking guards.'.format(self.id))
        valid = set()
        for input, action in self.input_actions:
            if input(self.active_msg, self):
                valid.add((input, action))
        return list(valid)

    def schedule(self, input_action):
        print('Task {} is now scheduling {}.'.format(self.id, input_action[1].__name__))
        self.scheduler.enter(5, 1, self.in_act_statement, (input_action[0], input_action[1]))
        self.scheduler.run()
        self.active_msg.data = None

    def in_act_statement(self, input, action):
            action(self.active_msg, self)
            self.active_msg.data = None

class Loadgenerator(Thread):
    def __init__(self, rate = 1, id = 'LG', out= None, LG_active = True, args = (), kwargs= None):
        Thread.__init__(self, args = args, kwargs = kwargs, name = id)
        self.rate = rate
        self.id = 'LG'
        self.out = out
        self.LG_active = LG_active

    def run(self):
        print('Loadgenerator is now active and sending load every {} seconds.'.format(self.rate))
        while True:
            dst = random.choice(self.out)
            self.send(Message(self, dst, 'load'), dst)
            sleep(self.rate)

    def send(self, msg, dst):
        dst.receive(msg)


class Message():
    def __init__(self, origin, dst, data):
        self.origin = origin
        self.dst = dst
        self.data = data
